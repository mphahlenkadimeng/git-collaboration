
#The responsibilities

 Facilitating (not participating in) the daily stand up

 Helping the team maintain their burn down chart

 Setting up retrospectives, sprint reviews or sprint planning sessions

 Shielding the team from interruptions during the sprint

 Removing obstacles that affect the team

 Walking the product owner through more technical user stories

 Encouraging collaboration on the Scrum team.